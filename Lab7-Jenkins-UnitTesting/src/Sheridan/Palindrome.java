package Sheridan;

public class Palindrome {

	public static void main(String[] args) {
		// TODO Auto-generated method stub]
		System.out.println("is anna palindrome?");
		Palindrome.isPalindrome("anna");
		System.out.println("is taco cat palindrome? - "+Palindrome.isPalindrome("taco cat"));
		System.out.println("is mark palindrome? - "+Palindrome.isPalindrome("mark"));
		

	}
	//merged
	public static boolean isPalindrome(String input) 
	{
		input = input.toLowerCase().replaceAll(" ", "");
		
		for(int i = 0, j = input.length() - 1; i < j; i++, j--) 
		{
			if(input.charAt(i) != input.charAt(j)) 
			{
				return false;
			}
		}
		
		
		return true;
	}

}
