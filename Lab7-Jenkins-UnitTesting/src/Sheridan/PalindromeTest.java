package Sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIsPalindrome() {
		assertTrue("Invalid palindrome", Palindrome.isPalindrome("Anna"));
	}
	
	@Test
	public void testIsPalindromeNotPalindrome() {
		assertFalse("Is palindrome", Palindrome.isPalindrome("Mark"));
	}
	@Test
	public void testIsPalindromeVeryCloseNotPalindrome() {
		assertFalse("Is palindrome", Palindrome.isPalindrome("Annas"));
	}
	@Test
	public void testIsPalindromeVeryCloseIsPalindrome() {
		assertTrue("Invalid palindrome", Palindrome.isPalindrome("123454321"));
	}
	

}
